# The Mageseeker: A League of Legend Story

* 🖥️ Full gameplay functionality via Steam Proton.
* 🖥️ Available on [Steam](https://store.steampowered.com/app/1457080/The_Mageseeker_A_League_of_Legends_Story/) and [Epic Games](https://store.epicgames.com/en-US/p/the-mageseeker-a-league-of-legends-story/).
* 🔗 [ProtonDB](https://www.protondb.com/app/1457080), [main site](https://www.themageseeker.com/) 
* Elsewhere on reddit at r/mageseeker.